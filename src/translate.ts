import { Translate } from 'aws-sdk';
const translate = new Translate();

const SECRET_CODE = 'MAGIC_POSER_123';

export const translateObjectValues = async (event) => {
  try {
    const requestBody = JSON.parse(event.body);
    if (!requestBody) throw new Error('Please provide request body');
    console.log(requestBody);

    const secret = requestBody.secret;

    if (secret !== SECRET_CODE) throw new Error('Invalid Credentials');

    const obj = {};

    const objectToTranslate = requestBody.translate;
    const localesToTranslate = requestBody.locales;

    for (const prop in objectToTranslate) {
      if (objectToTranslate.hasOwnProperty) {
        const res = await translateString(objectToTranslate[prop], localesToTranslate);
        obj[prop] = res;
      }
    }

    return {
      statusCode: 200,
      body: JSON.stringify(obj),
    };
  } catch (err) {
    return {
      statusCode: 400,
      body: err.message,
    };
  }
};

const translateString = async (text: string, locales: string[]) => {
  const promiseArray = [];

  for (const locale of locales) {
    promiseArray.push(generateTranslatePromise(text, locale));
  }

  const result: Record<string, string>[] = await Promise.all(promiseArray);
  const obj = result.reduce((prev, curr) => ({ ...prev, ...curr }), {});
  return obj;
};

const generateTranslatePromise = (text: string, languageCode: string) => {
  if (text.trim() === '') return Promise.resolve({ [languageCode]: '' });
  return translate
    .translateText({
      SourceLanguageCode: 'en',
      // hack to fix inconsistency in language code between our FE and Amazon's API
      TargetLanguageCode: languageCode === 'zh-Hant' ? 'zh-TW' : languageCode,
      Text: text,
    })
    .promise()
    .then((val) => {
      return {
        [languageCode]: val.TranslatedText,
      };
    });
};
